#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#include <windows.h>
#include <math.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>

 int _tmain(int argc, _TCHAR* argv[]) 
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	printf("�����������:\t���������� ������\n");

	double a_1 = (double)0.0000007;
	double b_1 = (double)180000000;
	double c_1 = (double)0.0000187;
	double d_1 = (double)(pow(double(5),10)-18);
	double e_1 = (double)(0.1*10-10);
	double f_1 = (double)1.0004*10+18;

	printf("\n������� 1: �������� ����� �� �������� ���� � ������ � ��������� �����:\n");
	printf("a)0.0000007 = %f",a_1);
	printf("\nb)180000000 = %f",b_1);
	printf("\nc)0.0000187 = %f",c_1);
	printf("\nd)pow(5,10)-18 = %f",d_1);
	printf("\ne)0.1*10-10 = %f",e_1);
	printf("\nf)1.0004*10+18 = %f",f_1);

	double a_2 = (double)(0.1*pow(double(10),6));
	double b_2 = (double)(1.87*pow(double(10),-18));
	double c_2 = (double)(17*pow(double(10),3));
	double d_2 = (double)(0.14*pow(double(10),-8));
	double e_2 = (double)(11*pow(double(10),4));
	double f_2 = (double)(3*pow(double(10),-14));

	printf("\n\n������� 2: �������� ����� � ����������� ����:\n");
	printf("a)0.1E+6 = %f",a_2);
	printf("\nb)1.87E-18 = %f",b_2);
	printf("\nc)17E+3 = %f",c_2);
	printf("\nd)0.14E-8 = %f",d_2);
	printf("\ne)11E+4 = %f",e_2);
	printf("\nf)3E-14 = %f",f_2);

	printf("\n\n������� 3: ����� ������ � ���������: a) 15E6 �) 0.1�-58 �) cos3 �) E-5 �) 1.3E+39.1 �) E12 �)7�");
    printf("\n�����: ��������� ������ ���������� ��� ������� � � �. ������-��� ������� ������������ ����� ������ �� ��������� [-37,37].\n\n");

	system("PAUSE");
	return 0;
}
